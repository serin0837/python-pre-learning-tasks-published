import re


def vowel_swapper(string):
    # ==============
    # Your code here
    arrstr = string.split()
    for word in arrstr:
        return string.lower().replace("a", '4', 2).replace('4', 'a', 1).replace("e", "3", 2).replace("3", "e", 1).replace("o", "ooo", 2).replace("o", "ooo", 1).replace("u", "|_|", 2).replace("|_|", "u", 1) if word[0].isupper() else string.lower().replace("a", '4', 2).replace('4', 'a', 1).replace("e", "3", 2).replace("3", "e", 1).replace("o", "ooo", 2).replace("o", "ooo", 1).replace("u", "|_|", 2).replace("|_|", "u", 1)

    # ============== can not make upper case or lower case


# print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
# print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
# print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console

# Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("aAa eEe iIi oOo uUu"))
# Should print "Hello Wooorld" to the console
print(vowel_swapper("Hello World"))
# Should print "Ev3rything's Av/\!lable" to the console
print(vowel_swapper("Everything's Available"))
